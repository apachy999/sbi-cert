FROM nginx

RUN apt-get update && apt-get install -y openssl

COPY conf.d/myserver.conf /etc/nginx/conf.d/default.conf
COPY ssl/ /etc/nginx/ssl
COPY entrypoint.sh /entrypoint.sh

#EXPOSE 880
EXPOSE 8443

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
