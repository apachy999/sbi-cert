#!/bin/bash

if ! openssl x509 -checkend 0 -noout -in /etc/nginx/ssl/mycert.crt; then
    echo "Certificate is expired!"
    exit 1
fi

exec nginx -g 'daemon off;'
